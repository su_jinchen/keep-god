package com.kbg.it.platform.base;

import com.kbg.it.platform.enums.ErrorCode;

/**
 * 描述: 统一的响应实体
 *
 * @author : bins
 * @date : 2022/3/18
 */
public class ResponseResult<T> {
    public static final String SUCCESS = "success";

    public static final String FAIL = "fail";

    private String success;

    private String code;

    private String message;

    private T data;

    /**
     * 成功的构造（带data数据）
     *
     * @param data
     */
    public ResponseResult(T data) {
        this.success = SUCCESS;
        this.code = ErrorCode.SUCCESS.getCode();
        this.message = ErrorCode.SUCCESS.getMessage();
        this.data = data;
    }

    /**
     * 成功的构造（无ata数据）
     */
    public ResponseResult() {
        this.success = SUCCESS;
        this.code = ErrorCode.SUCCESS.getCode();
        this.message = ErrorCode.SUCCESS.getMessage();
    }

    /**
     * 失败的构造（失败的错误码、错误码信息）
     *
     * @param code
     * @param message
     */
    public ResponseResult(String code, String message) {
        this.success = FAIL;
        this.code = code;
        this.message = message;
    }

    public static String getSUCCESS() {
        return SUCCESS;
    }

    public static String getFAIL() {
        return FAIL;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "success='" + success + '\'' +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
