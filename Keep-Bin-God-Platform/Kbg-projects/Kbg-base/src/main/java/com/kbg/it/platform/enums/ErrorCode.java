package com.kbg.it.platform.enums;

/**
 * 描述: 通用错误码
 *
 * @author : bins
 * @date : 2022/3/18
 */
public enum ErrorCode {
    //成功的状态码
    SUCCESS("0000", "success"),
    //系统内部错误状态码
    INTERNAL("system.internal.error", "System internal error");


    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
