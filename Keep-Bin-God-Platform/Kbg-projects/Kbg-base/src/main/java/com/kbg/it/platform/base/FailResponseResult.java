package com.kbg.it.platform.base;

/**
 * 描述: 失败的响应体
 *
 * @author : bins
 * @date : 2022/3/19
 */
public class FailResponseResult extends ResponseResult{

    public FailResponseResult(String code, String message) {
        super(code, message);
    }
}
