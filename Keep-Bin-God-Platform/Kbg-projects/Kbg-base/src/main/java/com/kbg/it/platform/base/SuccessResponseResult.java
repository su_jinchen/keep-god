package com.kbg.it.platform.base;

/**
 * 描述: 成功的响应体
 *
 * @author : bins
 * @date : 2022/3/19
 */
public class SuccessResponseResult<T> extends ResponseResult<T> {
    public SuccessResponseResult() {
    }

    public SuccessResponseResult(T data) {
        super(data);
    }
}
