package com.kbg.it.platform.base;

import com.kbg.it.platform.base.FailResponseResult;
import com.kbg.it.platform.base.SuccessResponseResult;
import com.kbg.it.platform.enums.ErrorCode;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述:
 *
 * @author : bins
 * @date : 2022/3/19
 */
public class ResponseResultTest {

    @Test
    public void testSuccess() {
        System.out.println(new SuccessResponseResult<>());
        List list = new ArrayList();
        list.add(1);
        list.add(2);
        System.out.println(new SuccessResponseResult<>(list));
    }

    @Test
    public void testFail() {
        System.out.println(new FailResponseResult(ErrorCode.INTERNAL.getCode(),ErrorCode.INTERNAL.getMessage()));
    }
}